package com.nurlan.demo.security.jwt;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomNullPointerException extends NullPointerException{
    public CustomNullPointerException() {
        super();
    }

    public CustomNullPointerException(String s) {
        super(s);
    }
}
