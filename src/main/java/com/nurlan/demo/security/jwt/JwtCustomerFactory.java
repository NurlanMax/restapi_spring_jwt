package com.nurlan.demo.security.jwt;

import com.nurlan.demo.model.Customer;
import com.nurlan.demo.model.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JwtCustomerFactory {
    public static JwtCustomer create(Customer customer) {
        return new JwtCustomer(
                customer.getId(),
                customer.getUsername(),
                customer.getPassword(),
                mapToGrantedAuthorities(new ArrayList<>(customer.getRoles()))
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> customerRoles) {
        return customerRoles.stream()
                .map(role ->
                        new SimpleGrantedAuthority(role.getName())
                ).collect(Collectors.toList());
    }

}

