package com.nurlan.demo.security;

import com.nurlan.demo.model.Customer;
import com.nurlan.demo.security.jwt.JwtCustomer;
import com.nurlan.demo.security.jwt.JwtCustomerFactory;
import com.nurlan.demo.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JwtCustomerDetailsService implements UserDetailsService {

    private final CustomerService customerService;

    @Autowired
    public JwtCustomerDetailsService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerService.findByUsername(username);
        if (customer == null) {
            throw new UsernameNotFoundException("cutomer with username: " + username + " not found");
        }
        JwtCustomer jwtCustomer= JwtCustomerFactory.create(customer);
        log.info("IN loadUserByUsername - user with username: {} successfully loaded", username);
        return jwtCustomer;
    }
}
