package com.nurlan.demo.repository;


import com.nurlan.demo.model.Customer;
import com.sun.org.apache.xpath.internal.objects.XBoolean;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Repository inter
 */

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findByUsername(String name);
    boolean existsById(Long id);
}
