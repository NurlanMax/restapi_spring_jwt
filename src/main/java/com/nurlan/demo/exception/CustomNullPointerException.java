package com.nurlan.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomNullPointerException extends NullPointerException {
    public CustomNullPointerException() {
    }

    public CustomNullPointerException(String s) {
        super(s);
    }
}
