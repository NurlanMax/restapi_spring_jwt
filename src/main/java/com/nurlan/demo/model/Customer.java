package com.nurlan.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "customers")
@Data
public class Customer{
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;
        @Column(name = "username",nullable = false, unique = true)
        private String username;
        @Column(name = "password",nullable = false)
        private String password;
        @ManyToMany(fetch = FetchType.EAGER)
        @JoinTable(name = "customers_roles",
                joinColumns = @JoinColumn(
                        name="customers_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(
                        name = "role_id", referencedColumnName = "id"))
        private List<Role> roles;

    public Customer(){
    }

    public Customer(Long id, List<Role> roles) {
        this.id = id;
        this.roles = roles;
    }

    public Customer(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}
