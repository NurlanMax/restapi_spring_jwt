package com.nurlan.demo.rest;

import com.nurlan.demo.dto.CustomerListDTO;
import com.nurlan.demo.dto.CustomerRequestDto;
import com.nurlan.demo.model.Customer;
import com.nurlan.demo.security.JwtCustomerDetailsService;
import com.nurlan.demo.security.jwt.JwtCustomer;
import com.nurlan.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/customers")
public class CustomerController {
    private final CustomerService customerService;
    private final JwtCustomerDetailsService customerDetailsService;
@Autowired
    public CustomerController(CustomerService customerService, JwtCustomerDetailsService customerDetailsService) {
        this.customerService = customerService;
        this.customerDetailsService = customerDetailsService;
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<CustomerRequestDto> getUserById(@PathVariable(name = "id") Long id){
        Customer customer = customerService.getById(id);
        if(customer == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        CustomerRequestDto result = CustomerRequestDto.fromCustomer(customer);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listCustomers() {
        List<CustomerListDTO> customers = this.customerService.getAll();
        if (customers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

}

