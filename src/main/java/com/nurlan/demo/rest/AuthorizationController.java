package com.nurlan.demo.rest;

import com.nurlan.demo.dto.AuthenticationRequestDto;
import com.nurlan.demo.dto.CustomerDTO;
import com.nurlan.demo.model.Customer;
import com.nurlan.demo.security.jwt.JwtAuthenticationException;
import com.nurlan.demo.security.jwt.JwtTokenProvider;
import com.nurlan.demo.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/auth")
@Slf4j

public class AuthorizationController {
    @Value("${jwt.token.expired}")
    private long validityInMilliseconds;

    @Value("${jwt.refreshToken.expired}")
    private long refreshValidityInMilliseconds;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final CustomerService customerService;

    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AuthorizationController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, CustomerService customerService, BCryptPasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.customerService = customerService;
        this.passwordEncoder = passwordEncoder;
    }


    @PostMapping("/registration")
    public ResponseEntity<?> addCustomer(@RequestBody CustomerDTO request) {
        customerService.save(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequestDto requestDto) {
        try {
            String username = requestDto.getUsername();
            log.info("Get username :{}", username);
            log.info("123", authenticationManager.authenticate
                    (new UsernamePasswordAuthenticationToken(username, requestDto.getPassword())));
            authenticationManager.authenticate
                    (new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            log.info("Authenticate :{}", requestDto.getPassword());
            Customer customer = customerService.findByUsername(username);
            log.info("Find user by username :{}", customer);
            if (customer == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }

            String token = jwtTokenProvider.createToken(username, customer.getRoles(), validityInMilliseconds);
            String refreshToken = jwtTokenProvider.createToken(username, customer.getRoles(), refreshValidityInMilliseconds);
            log.info("Create token :{}", token);
            Map<Object, Object> response = new HashMap<>();
            log.info("Put data in token");
            response.put("username", username);
            response.put("token", token);
            response.put("refreshToken", refreshToken);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new JwtAuthenticationException("Invalid username or password");
        }
    }
}



