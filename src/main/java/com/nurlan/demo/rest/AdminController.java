package com.nurlan.demo.rest;

import com.nurlan.demo.dto.CustomerDTO;
import com.nurlan.demo.dto.CustomerListDTO;
import com.nurlan.demo.dto.UpdateCustomerDTO;
import com.nurlan.demo.model.Customer;
import com.nurlan.demo.security.JwtCustomerDetailsService;
import com.nurlan.demo.security.jwt.JwtCustomer;
import com.nurlan.demo.service.CustomerService;
import org.hibernate.sql.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/admin")
public class AdminController {
    private final CustomerService customerService;
    private final JwtCustomerDetailsService customerDetailsService;

    @Autowired
    public AdminController(CustomerService customerService, JwtCustomerDetailsService customerDetailsService) {
        this.customerService = customerService;
        this.customerDetailsService = customerDetailsService;
    }

    @PostMapping("/registration")
    public ResponseEntity<?> addAdmin(@RequestBody CustomerDTO request) {
        customerService.saveAsAdmin(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listCustomers() {
        List<CustomerListDTO> customers = this.customerService.getAll();
        if (customers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @PostMapping("/update/{id}")
    public void updateCustomer(@PathVariable Long id, @RequestBody UpdateCustomerDTO customer){
        System.err.println(customer);
        customer.setId(id);
        customerService.update(customer);
    }
    @PostMapping("/delete/{id}")
    public void deleteCustomer(@PathVariable("id") Long id){
        customerService.delete(id);
    }
}

