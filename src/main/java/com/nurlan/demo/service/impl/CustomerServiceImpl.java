package com.nurlan.demo.service.impl;


import com.nurlan.demo.exception.CustomNullPointerException;
import com.nurlan.demo.dto.CustomerDTO;
import com.nurlan.demo.dto.CustomerListDTO;
import com.nurlan.demo.dto.UpdateCustomerDTO;
import com.nurlan.demo.model.Customer;
import com.nurlan.demo.model.Role;
import com.nurlan.demo.repository.CustomerRepository;
import com.nurlan.demo.repository.RoleRepository;
import com.nurlan.demo.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {
    private final RoleRepository roleRepository;
    private final CustomerRepository customerRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;
    @Autowired
    public CustomerServiceImpl(RoleRepository roleRepository, CustomerRepository customerRepository, BCryptPasswordEncoder passwordEncoder, ModelMapper modelMapper) {
        this.roleRepository = roleRepository;
        this.customerRepository = customerRepository;
        this.passwordEncoder = passwordEncoder;
        this.modelMapper = modelMapper;
    }


    @Override
    public Customer getById(Long id) {
        Customer result=customerRepository.findById(id).orElseThrow(() -> new CustomNullPointerException("No value present"));

        if(result==null) {
            log.warn("IN getById - no user found by id: {}", id);
            return null;
        }
        log.info("IN getById - user: {} found by id: {}", result);
        return result;
    }

    @Override
    public void save(CustomerDTO request) {
        Role roleCustomer=roleRepository.findByName("ROLE_CUSTOMER");
        List<Role> customerRole=new ArrayList<>();
        customerRole.add(roleCustomer);
        Customer customer = new Customer();
        customer.setUsername(request.getUsername());
        customer.setPassword(passwordEncoder.encode(request.getPassword()));
        customer.setRoles(customerRole);
        customerRepository.save(customer);
        //if(userServiceImpl.findUserByUsername(request.getUsername() != null) throw new ResponseStatusException(HttpStatus.IM_USED, "not unique");
        log.info("IN register-customer: {} successfully registered", customer);
    }

    @Override
    public void saveAsAdmin(CustomerDTO request) {
        Role roleCustomer=roleRepository.findByName("ROLE_ADMIN");
        List<Role> customerRole=new ArrayList<>();
        customerRole.add(roleCustomer);
        Customer customer = new Customer();
        customer.setUsername(request.getUsername());
        customer.setPassword(passwordEncoder.encode(request.getPassword()));
        customer.setRoles(customerRole);
        customerRepository.save(customer);
        //if(userServiceImpl.findUserByUsername(request.getUsername() != null) throw new ResponseStatusException(HttpStatus.IM_USED, "not unique");
        log.info("IN register-admin: {} successfully registered", customer);
    }


    @Override
    public void delete(Long id) {
        if(!customerRepository.existsById(id)){
            throw new CustomNullPointerException("User not found by id");
        }
        customerRepository.deleteById(id);
        log.info("IN delete - customer with id: {} successfully deleted", id);
    }



    @Override
    public List<CustomerListDTO> getAll() {
        List<Customer> customers = customerRepository.findAll();
        List<CustomerListDTO> customerListDTOS = customers
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
        log.info("IN getAll - {} customers found", customerListDTOS.size());
        return customerListDTOS;
    }


    @Override
    public void update(UpdateCustomerDTO customer) {
        Role role = roleRepository.findByName(customer.getRoles().getName());
        Customer currentCustomer = getById(customer.getId());
        currentCustomer.setUsername(customer.getUsername());
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        currentCustomer.setRoles(roles);
        customerRepository.save(currentCustomer);
    }

    private CustomerListDTO convertToDTO(Customer customer){
        return modelMapper.map(customer,CustomerListDTO.class);
    }

    @Override
    public Customer findByUsername(String username) {
        Customer result=customerRepository.findByUsername(username);
        log.info("IN findByUsername - user: {} found by username: {}",result,username);
        return result;
    }
}





//    @Override
//    public List<Customer> getAll() {
//        List<Customer> result=customerRepository.findAll();
//        log.info("IN getAll - {} customers found", result.size());
//        return result;
//    }




//    @Override
//    public Customer update(Customer customer) {
//        System.err.println(customer);
//        return customerRepository.save(customer);
//    }
