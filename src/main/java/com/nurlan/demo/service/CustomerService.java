package com.nurlan.demo.service;

import com.nurlan.demo.dto.CustomerDTO;
import com.nurlan.demo.dto.CustomerListDTO;
import com.nurlan.demo.dto.UpdateCustomerDTO;
import com.nurlan.demo.model.Customer;

import java.util.List;

public interface CustomerService {
    Customer getById(Long id);
    void save(CustomerDTO request);
    void saveAsAdmin(CustomerDTO request);
    void delete(Long id);
    List<CustomerListDTO> getAll();
    void update(UpdateCustomerDTO customer);
    Customer findByUsername(String username);
}
