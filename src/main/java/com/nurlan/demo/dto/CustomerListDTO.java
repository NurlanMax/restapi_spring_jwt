package com.nurlan.demo.dto;

import com.nurlan.demo.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.asm.Advice;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerListDTO {
    private Long id;
    private String username;
    private List<RoleDTO> roles;
}
