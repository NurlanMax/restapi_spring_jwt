package com.nurlan.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCustomerDTO {
    private Long id;
    private String username;
    private RoleDTO roles;
}
