package com.nurlan.demo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nurlan.demo.model.Customer;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRequestDto {
    private Long id;
    private String username;

    public Customer toCustomer(){
        Customer customer=new Customer();
        customer.setId(id);
        customer.setUsername(username);
        return customer;
    }



    public static CustomerRequestDto fromCustomer(Customer customer){
        CustomerRequestDto customerDto=new CustomerRequestDto();
        customerDto.setId(customer.getId());
        customerDto.setUsername(customer.getUsername());
        return customerDto;
    }
}