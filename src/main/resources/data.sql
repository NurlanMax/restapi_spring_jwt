INSERT INTO roles(id, name)
values (1, 'ROLE_CUSTOMER');
Insert into roles(id, name)
values (2, 'ROLE_ADMIN');
INSERT INTO customers(password, username)
values ('$2y$12$Z7YZ/mcNQjX/ClTzEuNIgeeB24EoFZlBncgucKABAbkjN.aytXBwq','admin');
INSERT INTO customers_roles(customers_id, role_id)
values (1,2);
