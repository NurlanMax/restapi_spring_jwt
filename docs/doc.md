## API documentation for registration and authorization
### Table of contents

- [Login](#login)
- [Registration](#registration)
### Login

#### Description
Sign up users

#### HTTP request

````
POST /api/v1/auth/login

Content-Type: application/json
Required token: no
````

#### HTTP body request params
````
    "username": //String,
    "password": //String
````

#### Sample body request
````
"username": "admin"
"password": "admin"
````



#### Errors

````
    No Errors
````

### Registration

#### Description
Registration users

#### HTTP request

````
POST /api/v1/auth/registration

Content-Type: application/json
Required token: no
````

#### HTTP body request params
````
    "username": //String,
    "password": //String
````

#### Sample body request
````
    "username": admin //String
    "password": admin //String
````

#### Sample response

````
200 OK
````

#### Errors

````
    No Errors
````
### Find by user ID

